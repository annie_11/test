# test

0201

# 獻給 PJM 的 Git 極簡教學 
# Git 新手教學 

以下將會簡述git常用語法與概念，主要是面向PJM與新手\
by ytchen at 2020/10/16

## git是什麼？
文件基本推播

當你或是你家的工程師與別人協作時常會遇到一些問題，\
像是A寫好了結果B也寫了另一個版本、大家based on的版本有新有舊等令人崩潰的情況。

這時就需要git，git是最大眾的分散式版本控制軟體，總之就是一個方便版控的好東西！\
常見的github跟gitlab都是使用git這種版控工具遠端託管code的平台，讓你可以用圖形化的介面輕鬆與其他人進行愉快 ( ? ) 的協作囉 ~

## git 概念

### 本機 (Local):
本機簡單來說就是你所用的電腦，單機版的只會由你開發，但是也可以使用git進行版控 

1. 叫git來管理這個資料夾
``` 
git init                     #初始化git
```
此時你指定的資料夾會多出一個名為「.git」的隱藏資料夾

git會自動幫你建立名為**master**的主要分支，通常是默認為最重要的分支，最終各分支會合併到**master**

2. 將要被版控的東西放進暫存區吧
```
git add -A                    #將資料夾的東西通通放進暫存區
```
暫存區是還沒提交(commit)的地方，在這裡你可以盡情修改\
就像是考試拿到考卷的期間你可以把答案改A改B改C等等

當然你也可以只增加一個檔案
```
git add {檔名}                 #將{檔名}放進暫存區
```
這邊建議養成好習慣，隨時檢視一下目前暫存區的狀態(status)，也就是你目前對他add或修改了什麼
```
git status                      #檢視暫存區目前狀態
```

3. 提交此次變動

```
git commit -m "描述"            #這次版本做了什麼
```
commit如字面上意思，會將暫存區內的所有變動提交，\
就像是遊戲存檔，被玩壞之後隨時還能再load回來這個版本

### 遠端 (Remote) : 
遠端就能透過github、gitlab等平台透過git進行多人協作囉 ~ 
git status                    #可以持續追蹤目前狀態

為了簡化流程，本篇不會提到如何自己建立repository也不會提到設定ssh等。
git add {README.md}           #儲存這次的變動

如果這專案連個repository都還沒有那就請工程師幫忙開吧 XD
1. 設定個人資料檔
git commit -m "補充這次的變動"  #
```
git config --global user.name <username>
git config --global user.email <mailaddress>
#### 
```
為了連到remote，必須設定好名字跟email，\
這邊email設定成公司的email就好了，名字避免麻煩可以設定成帳號名\
因為當初申辦gitlab跟github時都是用data-sci.info的email，只需要設定一次
git push {origin master}          #推到 master

2. 複製一份專案資料夾
``` 
git clone {網址}                 #複製一份專案資料夾到本機上
git push {origin master:{kelly}}  #推到 自定義的新kelly bench 
```
網址可以透過github/gitlab平台上的clone取得，\
如果不知道或是沒有設定ssh那就用HTTPS的網址就好

沒有clone的選項那就必須先fork一份到自己的帳號底下，\
通常只有gitlab會發生這種情況，遇到再call工程師
![](img/clone.PNG)
從遠端的server clone下來時，預設的remote節點就會叫做**origin**，\
當然如果不喜歡也可以改但沒什麼必要

3. 開一個屬於自己的分支 ( branch ) \
由於目前(2020/10)本公司決定開發流程遵循git flow，所以團隊的每個人都必須開一個分支進行開發，合併時發PR ( Pull Request )\
亂推master一定有風險，分支的命名規則請參考當下文件
```
git checkout -b {branch name}     #開新分支並切換到該分支
```
建立成功的話路徑的括號中間會變成你所取的分支名稱，這時即可放心使用

![](img/checkout.png)

4. 更改或增加所要的東西\
例如PJM撰寫的文件需要丟上去，那就必須先在本機端add與commit\
忘記的請拉上去重看，因為是既有的專案，所以不用再下init的指令喔！

5. { 更新到remote的進度吧 }\
在Local上醞釀已久的大作終於可以讓團隊的大家看到了\
但是如果寫了很久的話可能會有人搶先一步推到remote\
以防萬一，先fetch一下
```
git fetch                         #抓取現在remote端的進度
```
![](img/fetch.png)
可以看到說local端很久沒更新，團隊不僅新增了3條branch，連master的版本都改變了\
注意，fetch只會告訴你git的進度，不會自動幫你更新！

由於開發流程不允許使用pull(fetch+merge)，因此這時需要做的事是**rebase**，\
rebase可以想成把based on舊版本(目前的local端)換成新版本(remote版本)，
```
git rebase origin                 #將版本換為remote端的
```
成功rebase並更新local端進度後會恭喜你
![](img/rebase1.png)

但理論上現在的分支只會有你一個人開發，\
所以應該是everything is up to date的

![](img/rebase2.png)

6. 推 ( push ) 到remote去吧\
不想要像上述這麼麻煩的話還是今日事今日畢的趕快寫好或是遵守git flow(X)

如果是第一次開branch的話remote會請你輸入
```
git push --set-upstream origin {branch name}
```
![](img/upstream.PNG)
這個意思是因為你原本開分支是在local，現在要推上remote的話就需要先upstream它，\
總之就跟著提示打就對了
```
git push origin                   #推到名為origin的remote節點
```
這會將你在本機端的commit push到遠端，也就是github或gitlab
![](img/push.png)

---
p.s. 實在不懂的話可以使用 [github Desktop](https://desktop.github.com/) 
